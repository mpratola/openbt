
## Welcome to the OpenBT (Open Bayesian Trees) project

OpenBT is a flexible, extensible and parallel C++ framework for implementing Bayesian regression tree models.  

For binary package downloads and basic examples in R, see the [Wiki](https://bitbucket.org/mpratola/openbt/wiki/Home).


