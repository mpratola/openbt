//     rrn.h: Random number generator virtual class definition.
//     Copyright (C) 2012-2019 Matthew T. Pratola
//
//     This file is part of OpenBT.
//
//     OpenBT is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     OpenBT is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact information
//     Matthew T. Pratola: mpratola@gmail.com


#ifndef RRN_H
#define RRN_H

//extern "C" {
#include <R.h>
#include <Rmath.h>
//};

#include "rn.h"

class rrn: public rn
{
public:
//constructor
   rrn():df(1),alpha(1.0),beta(1.0) {}
//virtual
   virtual ~rrn() {}
   virtual double normal() {return norm_rand();}
   virtual double uniform() { return unif_rand();}
   virtual double chi_square() {return rchisq((double)df);}
   virtual double gamma() {return rgamma(alpha,1.0/beta);} //R uses scale so 1/beta
   virtual void set_df(int df) {this->df=df;}
   virtual void set_gam(double alpha,double beta) {this->alpha=alpha; this->beta=beta;}
//get,set
   int get_df() {return df;}
private:
   int df;
   double alpha;
   double beta;
};

#endif
